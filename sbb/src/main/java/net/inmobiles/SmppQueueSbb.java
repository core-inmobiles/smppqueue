package net.inmobiles;

import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeoutException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.*;

import org.apache.log4j.Logger;
import org.mobicents.slee.*;
import org.restcomm.slee.resource.smpp.SmppSessions;
import org.restcomm.slee.resource.smpp.SmppTransaction;
import org.restcomm.slee.resource.smpp.SmppTransactionACIFactory;
import org.restcomm.smpp.Esme;

import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;
//import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public abstract class SmppQueueSbb implements Sbb, SmppQueue {

	// SMPP
	private SmppTransactionACIFactory smppServerTransactionACIFactory;
	private SmppSessions smppServerSessions;

	// RabbitMQ
	public static Connection connection;
	public static Channel channel;

	// private String reqQueueName = "txn_queue";
	public final static String RABBIT_SET_KEY_QUEUENAME = System.getenv("RABBIT_SET_KEY_QUEUENAME");
	public final static String RABBIT_SET_KEY_IP = System.getenv("RABBIT_SET_KEY_IP");
	public final static String RABBIT_SET_KEY_USER = System.getenv("RABBIT_SET_KEY_USER");
	public final static String RABBIT_SET_KEY_PASS = System.getenv("RABBIT_SET_KEY_PASS");

	// UTIL
	public static Logger log = Logger.getLogger(SmppQueueSbb.class);
	// public final static Logger logCDR = Logger.getLogger("CDRLogs");
	// public static SmppSession smppLevelSession = null;
	ScheduledExecutorService scheduler;

	// TODO: Perform further operations if required in these methods.
	public void setSbbContext(SbbContext context) {
		this.sbbContext = (SbbContextExt) context;
		try {

			Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

			// SMPP
			this.smppServerSessions = (SmppSessions) myEnv.lookup("slee/resources/smpp/server/1.0/provider");

			this.smppServerTransactionACIFactory = (SmppTransactionACIFactory) myEnv
					.lookup("slee/resources/smppp/server/1.0/acifactory");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Could not set SBB context and Link Factory: ", e);
		}
	}

	public void unsetSbbContext() {
		this.sbbContext = null;
	}

	// TODO: Implement the lifecycle methods if required
	public void sbbCreate() throws javax.slee.CreateException {
	}

	public void sbbPostCreate() throws javax.slee.CreateException {
	}

	public void sbbActivate() {
	}

	public void sbbPassivate() {
	}

	public void sbbRemove() {
	}

	public void sbbLoad() {
	}

	public void sbbStore() {
	}

	public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface activity) {
	}

	public void sbbRolledBack(RolledBackContext context) {
	}

	/**
	 * Convenience method to retrieve the SbbContext object stored in setSbbContext.
	 * 
	 * TODO: If your SBB doesn't require the SbbContext object you may remove this
	 * method, the sbbContext variable and the variable assignment in
	 * setSbbContext().
	 *
	 * @return this SBB's SbbContext object
	 */

	protected SbbContextExt getSbbContext() {
		return sbbContext;
	}

	private SbbContextExt sbbContext; // This SBB's SbbContext

	public void onServiceStartedEvent(javax.slee.serviceactivity.ServiceStartedEvent event,
			ActivityContextInterface aci/* , EventContext eventContext */) {

		log.info("------------ Service started ------------");
		connectRabbit();

	}

	public void onSUBMIT_SM(com.cloudhopper.smpp.pdu.SubmitSm event,
			ActivityContextInterface aci/* , EventContext eventContext */) {

		log.info("------------- SmppRoutingSbb onSUBMIT_SM ------------------");

		// Incoming Esme
		SmppTransaction smppServerTransaction = (SmppTransaction) aci.getActivity();
		Esme inEsme = smppServerTransaction.getEsme();

		// Variable
		String destAddress = event.getDestAddress().getAddress();
		String sourcAddress = event.getSourceAddress().getAddress();
		byte[] message = event.getShortMessage();
		String msgStr = new String(message);
		String msgChar = countChar(msgStr);
		String rabbitMessage = "";

		// GET Incoming NetworkId
		int incomingNetworkID = inEsme.getNetworkId();
		String inNetworkId = String.valueOf(incomingNetworkID);

		log.info("Received message: " + sourcAddress + "," + destAddress + "," + msgStr + "," + msgChar + ","
				+ inNetworkId);

		// Send ReqQueue
		rabbitMessage = sourcAddress + "," + destAddress + "," + msgStr + "," + countChar(msgStr) + "," + inNetworkId;
		sendReqQueue(rabbitMessage, channel, RABBIT_SET_KEY_QUEUENAME);

	}

	public static String countChar(String msg) {
		int cnt = 0;
		for (int i = 0; i < msg.length(); i++) {
			// if (Character.isWhitespace(msg.charAt(i))) {

			// } else {
			cnt += 1;
			// }
		}
		String strCnt = Integer.toString(cnt);
		return strCnt;

	}

	public static void sendReqQueue(String message, Channel channel, String rqn) {

		try {

			// send message with RequestQuename
			channel.queueDeclareNoWait(rqn, true, false, false, null);
			channel.basicPublish("", rqn, null, message.getBytes("UTF-8"));
			log.info("---------- RabbitMq RequestQueue Sent  -----------");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("IOException: " + e);
		}

	}

	public static void connectRabbit() {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(RABBIT_SET_KEY_IP);
		factory.setUsername(RABBIT_SET_KEY_USER);
		factory.setPassword(RABBIT_SET_KEY_PASS);
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			log.info("---------- RabbitMq Connection Started  -----------");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("An error has occured: IOException" + e);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			log.error("An error has occured: TimeoutException" + e);
		}

	}

}
